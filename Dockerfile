FROM ubuntu:18.04

WORKDIR /app

RUN dpkg --add-architecture i386 && apt-get update 

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Africa/Kampala

RUN apt-get install -y git curl gnupg unixodbc-dev unixodbc-dev:i386 \
  libodbc1:i386 odbcinst1debian2:i386 build-essential python3 \
  mono-devel wine64 ca-certificates-mono gcc-multilib g++-multilib

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
  && curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list \ 
  && curl -sL https://deb.nodesource.com/setup_16.x -o /tmp/nodesource_setup.sh \
  && bash /tmp/nodesource_setup.sh

RUN ACCEPT_EULA=Y apt-get install -y msodbcsql17 mssql-tools nodejs 

RUN curl -O http://www.unixodbc.org/unixODBC-2.3.11.tar.gz && \
  gunzip unixODBC-2.3.11.tar.gz && \
  tar xvf unixODBC-2.3.11.tar && \
  cd unixODBC-2.3.11 && \
  chmod +x ./configure && \
  ./configure && \
  CFLAGS=-m32 LDFLAGS=-m32 CXXFLAGS=-m32 make && \
  CFLAGS=-m32 LDFLAGS=-m32 CXXFLAGS=-m32 make install

RUN apt-get clean && rm -rf /var/lib/apt/lists/*